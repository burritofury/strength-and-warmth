;(function($) {
  if($('#chartAverage').length > 0) {
    var count = 0;
    var averageChart = $('#chartAverage').highcharts({
      chart: {
        type: 'scatter',
        animation:false,
        renderTo: 'container',
        cursor: 'pointer',
        allowPointSelect:true,
      },
      credits: {
        enabled: false
      },
      tooltip: {
          shared: true,
          useHTML: true,
          formatter: function() {
            if(this.point.comments != null) {
            var result = '<div style="width:125px;min-height:40px"><p><b>Comment</b></p>';
              result += '<p style="display:block;white-space:normal;line-height:1.2;">' + this.point.comments + '</p>';
            } else if(this.point.series.name != null && this.point.series.name == 'Average Score') {
              var result = '<div style="width:125px;"><p><b>' + this.point.series.name + '</b></p>';
            }

            result += '</div>';

            return result;
          },
          style: {
            width: '125px'
          }
      },
      title: {
        text: ''
      },
      subtitle: {
        text: 'Average score in blue, individual votes are black.',
        x: 40,
        y: 0
      },
      xAxis: {
        title: {
            style: {
              color: '#000000'
            },
            text: 'Warmth'
        },
        min:0,
        max:10,
        lineWidth:1,
        plotLines: [{
            value: 5,
            width: 2,
            color: '#000000'
        }],
        labels: {
          formatter: function() {
              return this.value;
          }
        },
        categories: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
      },
      yAxis: {
        title: {
            style: {
              color: '#000000'
            },
            text: 'Strength'
        },
        min:0,
        max:10,
      },
      legend: {
        enabled: false
      },
      exporting: {
        enabled: false
      },
      plotOptions: {
        series: {
            lineWidth: 0,
        }
      },
      series: [{
        name: 'Average Score',
        data: [[xAxis,yAxis]]
        }, {
        data: allVotes
      }],
    }, function(chart) { // on complete
     
        var width = chart.plotBox.width / 2.0;
        var height = chart.plotBox.height / 2.0 + 1;
            
        chart.renderer.rect(chart.plotBox.x,                      
                            chart.plotBox.y, width, height, 1)
            .attr({
                fill: '#b3f87b',
                zIndex: 1
            })
            .add();

        chart.renderer.text(
            '<p><strong>Fear/Envy</strong></p>', 
            chart.plotBox.x + chart.plotLeft + 30, 
            chart.plotBox.y + chart.plotTop + 40
        ).attr({
            zIndex: 1
        }).add();

     chart.renderer.rect(chart.plotBox.x + width,                      
                            chart.plotBox.y, width, height, 1)
            .attr({
                fill: '#97d5ee',
                zIndex: 1
            })
            .add();
      
      chart.renderer.text(
            '<p><strong>Admiration</strong></p>', 
            chart.plotBox.x + width + chart.plotLeft + 30, 
            chart.plotBox.y + chart.plotTop + 40
        ).attr({
            zIndex: 1
        }).add();
   
    chart.renderer.rect(chart.plotBox.x,                      
                            chart.plotBox.y + height, width, height, 1)
            .attr({
                fill: '#f8807f',
                zIndex: 1
            })
            .add();

      chart.renderer.text(
            '<p><strong>Contempt/Disgust</strong></p>', 
            chart.plotBox.x + chart.plotLeft + 5, 
            chart.plotBox.y + height + chart.plotTop + 40
        ).attr({
            zIndex: 1
        }).add();
    
    chart.renderer.rect(chart.plotBox.x + width,                      
                            chart.plotBox.y + height, width, height, 1)
            .attr({
                fill: '#f5ee79',
                zIndex: 1
            })
            .add();

      chart.renderer.text(
            '<p><strong>Pity</strong></p>', 
            chart.plotBox.x + width + chart.plotLeft + 55, 
            chart.plotBox.y + height + chart.plotTop + 40
        ).attr({
            zIndex: 1
        }).add();
        
    });
  }



})(jQuery);