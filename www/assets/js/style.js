;(function ($) {
  // var people;
  $('input#livesearch').typeahead([{
    name: 'people',
    valueKey: 'name',
    local: people,
    template:'<div class="clearfix"><img src="{{small_path}}" class="img-thumbnail"><h3>{{name}}</h3></div>',
    engine: Hogan,
  }]);

  $('input#livesearch').bind('typeahead:selected', function(obj, datum) {
    $('#searchpeople #id').val(datum.id);
    $('#searchpeople #xAxis').val(datum.xAxis);
  });

  $('input#livesearch').focusout(function() {
    console.log($(this).parent().parent().parent().parent().attr());
  });

  var characters = 50;
  $("#counter").append("You have  <strong>"+ characters+"</strong> characters remaining");

  $('.comment').keyup(function () {
    if($(this).val().length > characters) {
      $(this).val($(this).val().substr(0, characters));
    } else {

      $("#counter").html("You have  <strong>" + (characters - $(this).val().substr(0, characters).length) + "</strong> characters remaining");
    }
  });

  $('.glyphicon.glyphicon-question-sign').popover({
    html: true,
    trigger: 'click',
    content: function () {
      return '<img src="/assets/images/ChartExplanation.jpg" style="width:250px; height:200px;"/>';
    }
  });

  $(window).resize(function() {
    height = chart.height
    width = $("#chartRow").width() / 2
    chart.setSize(width, height, doAnimation = true);
  });

  $('.table-fixed-header').fixedHeader();
})(jQuery);
