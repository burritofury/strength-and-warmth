<?php namespace Colorsynthetic\Imagine\Facades;
 
use Illuminate\Support\Facades\Facade;
 
class Imagine extends Facade {
  protected static function getFacadeAccessor()
  {
    return new \Colorsynthetic\Imagine\Services\Imagine;
  }
}