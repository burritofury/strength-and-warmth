<?php

class AuthController extends BaseController {
	/**
	 * Setup the index page to call the login form
	 *
	 * @return void
	 */
	public function getIndex()
	{
		// Redirect route
		Redirect::route('login');
	}

	/**
	 * Setup the index page to call the login form
	 *
	 * @return void
	 */
	public function getLogin()
	{
		// check to see if user is already logged in
		if( ! Auth::check())
		{
			// Redirect route
			return View::make('themes.search.auth.login', array(
				'url' 			=> $this->themeUrls,
				'title'			=> $this->pageTitle,
				));
		}
		else
		{
			$url = URL::action('AdminController@getDashboard');

			return Redirect::to($url)->with('message', 'You are already logged in to the system.');
		}
	}

	/**
	 * Setup the index page to call the login form
	 *
	 * @return void
	 */
	public function postLogin()
	{
		// get input values
		$credentials = array(
			'username'		=> Input::get('username'),
			'password'		=> Input::get('password')
		);

		$loggedin = Input::get('loggedin');

 		$rules = array(
    		'username' 	=> 'required|min:5',
    		'password' 	=> 'required|alpha_num|between:4,12',
    		);

    // We'll run validation in the controller for convenience
    // You should export this to the model, or a service
    $validator = Validator::make($credentials, $rules);

    // Checks the form, not the actual authentication attempt
		if($validator->fails())
		{
			$messages = $validator->messages();
		}
		else
		{
			// Log user in
			// else if failed redirect user to login
			if(Auth::attempt($credentials, $loggedin))
			{
				$url = URL::action('AdminController@getDashboard');

				return Redirect::to($url)->with('message', 'You have logged in successfully');
			}
			else
			{
				return Redirect::to('login')
            ->withInput()
            ->with('flashError', 'Your username/password combination was incorrect.');
			}
		}

		return Redirect::to('login')
			->withInput()
			->with('errorsUsername', $this->setClass($messages, 'username', 'has-error'))
			->with('errorsPassword', $this->setClass($messages, 'password', 'has-error'))
			->withErrors($validator);
	}

	/**
	 * Setup the index page to call the login form
	 *
	 * @return redirect
	 */
	public function getLogout()
	{
		Auth::logout();

		return Redirect::to('login');
	}
}
