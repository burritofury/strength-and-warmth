<?php

class SearchController extends BaseController
{

	public 				$restful = true;
	private 			$urlQueries = null;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		// get json output of people
		$json = Search::getJson();

		return View::make('themes.search.index', array(
				'url' 				=> $this->themeUrls,
				'title'				=> $this->pageTitle,
				'metaTitle'		=> $this->metaTitle,
				'metaImage'		=> $this->metaImage,
				'json'				=> $json
				));
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getResults($id = null, $name = null)
	{
		if (is_null($name)) {
			// get the name and seperate it.
			$name = explode(' ', Input::get('livesearch'));

			// check and see if there are more than two names
			if(array_key_exists(2, $name))
			{
				// combine the second and last name to search in lastname field
				$name[1] = $name[2];

				unset($name[2]);
			}

			// if last name does not exist, redirect
			if (!isset($name[1]))
			{
				// redirect to homepage
				return Redirect::to('/')
					->with('message', 'No results found for this search. <a href="/suggestions">Add this person <span class="glyphicon glyphicon-envelope"></span></a>')
					->with('errorCss', 'alert-danger');
			}
			else
			{
				// $id = People::where('firstname', 'LIKE', "%$name[0]%")->where('lastname', 'LIKE', "%$name[1]%")->pluck('id');

				// do a db search for the name
				$results = People::where('firstname', 'LIKE', "%$name[0]%")->where('lastname', 'LIKE', "%$name[1]%")->first();

				// if our second search returns null, redirect user
				if (is_null($results))
				{
					// redirect to homepage
					return Redirect::to('/')
						->with('message', 'No results found for this search. <a href="/suggestions">Add this person <span class="glyphicon glyphicon-envelope"></span></a>')
						->with('errorCss', 'alert-danger');
				}

				// if we have a user, set up a name for the url
				$fullname = $results->id . '/' . Str::slug($results->firstname . ' ' . $results->lastname, '-');
			}

			$url = 'results/' . $fullname;

		    // This is where you'd do SEO cleanup to remove special chars.
		    return Redirect::to($url);
		} else {
			$person = People::where('id', $id)->first();

			$id = $person->id;

			if($id)
			{
				// get person information from the db
				$person = People::find($id);

				// get json output of people
				$json = Search::getJson();

				$this->metaTitle = $person->firstname . ' ' . $person->lastname;

				$this->metaImage = URL::to($person->large_path);

				return View::make('themes.search.vote', array(
						'url' 				=> $this->themeUrls,
						'title'				=> $this->pageTitle,
						'metaTitle'		=> $this->metaTitle,
						'metaImage'		=> $this->metaImage,
						'person'			=> $person,
						'json'				=> $json
						));
			}
			else
			{
				// redirect to homepage
				return Redirect::to('/')
					->with('message', 'No results found for this search. <a href="/suggestions">Add this person <span class="glyphicon glyphicon-envelope"></span></a>')
					->with('errorCss', 'alert-danger');
			}
		}
	}

	/**
	 * Create json formatted code for use on search.
	 *
	 * @return json
	 */
	public function postResults()
	{
		// get all input fields submitted
		$input = Input::all();

		// create custom message
		$messages = array(
			'required' => 'Please vote on the graph below by clicking on a point.',
		);

		// set rules for validation
    $rules = array(
		'xAxis'			=> 'required'
		);

    // validate input data
    $validator = Validator::make($input, $rules, $messages);

		// get person information from the db
		$person = People::find($input['id']);

		// get json output of people
		$json = Search::getJson();

    // if validation fails
    if ($validator->fails())
    {
    	// back to form
      return Redirect::to('results/' . $input['id'])->withErrors($validator);
    }
    else
		{
			if($this->checkForDuplicateSubmissions($input['id']))
			{
				if(isset($input['yAxis']))
				{
					$insert = DB::table('coordinates')->insertGetId(
						array(
							'person_id'			=> $input['id'],
							'xAxis'					=> $input['xAxis'],
							'yAxis'					=> $input['yAxis'],
							'created_at'		=> Carbon::now(),
							'updated_at'		=> Carbon::now()
							)
					);

					$insertComments = DB::table('comments')->insertGetId(
						array(
							'coordinates_id'			=> $insert,
							'comments'						=> htmlspecialchars(str_replace('\'', '\\\'', $input['comment']), ENT_COMPAT, 'UTF-8'),
							'created_at'		=> Carbon::now(),
							'updated_at'		=> Carbon::now()
							)
					);

					// make sure to log vote
					DB::table('submissions')->insertGetId(
						array(
							'person_id'					=> $input['id'],
							'coordinates_id'		=> $insert,
							'comments_id'				=> $insertComments,
							'ip_address'				=> Request::getClientIp(),
							'created_at'				=> Carbon::now(),
							'updated_at'				=> Carbon::now()
							)
					);

					// redirect to homepage
					return Redirect::to('results/average/' . $input['id'])
						->with('message', 'Thank you for your vote.')
						->with('errorCss', 'alert-success');
				}
			}
			else
			{
				// redirect to homepage with error message
				return Redirect::to('results/average/' . $input['id'])
					->with('message', 'You have already voted on this person. View results below.')
					->with('errorCss', 'alert-warning');
			}

		return View::make('themes.search.vote', array(
				'url' 				=> $this->themeUrls,
				'title'				=> $this->pageTitle,
				'metaTitle'		=> $this->metaTitle,
				'metaImage'		=> $this->metaImage,
				'person'			=> $person,
				'json'				=> $json
				));
			}
	}


	/**
	 * Create json formatted code for use on search.
	 *
	 * @return view
	 */
	public function getAverageVotes($id)
	{
		// get person information from the db
		$person = People::find($id);

		// get all coordinates
		$coordinates = DB::table('coordinates')
			->leftJoin('comments', 'coordinates.id', '=', 'comments.coordinates_id')
			->select('coordinates.id', 'coordinates.xAxis', 'coordinates.yAxis', 'comments.comments')->where('person_id', $id)->get();

		// set variables for foreach loop
		$averageAxisX = 0;
		$averageAxisY = 0;
		$multipleCoordinates = null;

		$int = 0;
		$totalAxis = count($coordinates);

		foreach($coordinates as $coordinate)
		{
			// add all x axis together
			$averageAxisX = $averageAxisX + $coordinate->xAxis;

			// add all y axis together
			$averageAxisY = $averageAxisY + $coordinate->yAxis;

			// create x and y vote coordinates for use in highcharts
			$coordinatesModified = '{';
			$coordinatesModified .= 'x: ' . $coordinate->xAxis;
			$coordinatesModified .= ',';
			$coordinatesModified .= 'y: ' . $coordinate->yAxis;
			$coordinatesModified .= ',';
			$coordinatesModified .= 'comments: \'' . $coordinate->comments . '\'';
			$coordinatesModified .= '}';

			if ($int != $totalAxis - 1)
			{
				$coordinatesModified .= ',';
			}

			$multipleCoordinates .= $coordinatesModified;

			$int++;
		}

		// add closures to highcharts data format
		$coordinates = '[';
		$coordinates .= $multipleCoordinates;
		$coordinates .= ']';

		$countCoordinates = DB::table('coordinates')->select('coordinates.id','coordinates.xAxis','coordinates.yAxis')->where('person_id', $id)->get();

		if(count($countCoordinates) > 1)
		{
			// calculate the average vote
			$averageAxisX = number_format(((float)$averageAxisX / $totalAxis), 1, '.', '');
			$averageAxisY = number_format(((float)$averageAxisY / $totalAxis), 1, '.', '');
		}
		elseif(isset($countCoordinates[0]->xAxis) && isset($countCoordinates[0]->yAxis))
		{
			// calculate the average vote
			$averageAxisX = $countCoordinates[0]->xAxis;
			$averageAxisY = $countCoordinates[0]->yAxis;

			$coordinates = '[{}]';
		}
		else
		{
			$averageAxisX = NULL;
			$averageAxisY = NULL;
		}

		$quote = null;

		// set up quotes
		if($averageAxisX < 0 && $averageAxisY < 0)
		{
			$quote = array(
				'title' 	=> 'Learn more about people who project low strength and low warmth.',
				'url'			=> 'http://compellingpeople.com/low-strength-and-low-warmth/'
				);
		}
		else if($averageAxisX > 0 && $averageAxisY > 0)
		{
			$quote = array(
				'title' 	=> 'Learn more about people who project high strength and high warmth.',
				'url'			=> 'http://compellingpeople.com/high-strength-and-high-warmth/'
				);
		}
		else if($averageAxisX < 0 && $averageAxisY > 0)
		{
			$quote = array(
				'title' 	=> 'Learn more about people who project high strength and low warmth.',
				'url'			=> 'http://compellingpeople.com/high-strength-and-low-warmth/'
				);
		}
		else if($averageAxisX > 0 && $averageAxisY < 0)
		{
			$quote = array(
				'title' 	=> 'Learn more about people who project low strength and high warmth.',
				'url'			=> 'http://compellingpeople.com/low-strength-and-high-warmth/'
				);
		}
		else
		{
			$quote = array(
				'title' 	=> 'There are currently not enough votes to determine an average score.',
				'url'			=> NULL,
				);
		}

		// get json output of people
		$json = Search::getJson();

		// validator
		return View::make('themes.search.results', array(
				'url' 					=> $this->themeUrls,
				'title'					=> $this->pageTitle,
				'person'				=> $person,
				'json'					=> $json,
				'averageX'			=> $averageAxisX,
				'averageY'			=> $averageAxisY,
				'coordinates'		=> $coordinates,
				'metaTitle'			=> $this->metaTitle,
				'metaImage'			=> $this->metaImage,
				'quote'					=> $quote
				));
	}

	/**
	 * Create json formatted code for use on search.
	 *
	 * @return json
	 */
	public function getPeopleJson()
	{

		// get json output of people
		$json = Search::getJson();

		foreach ($search as $searched)
		{
			$data[] = array(
					'name'		=> $searched['name'],
					'path'		=> $searched['medium_path']
					);
		}

		return Response::json($data);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getSuggestions()
	{
		return View::make('themes.search.suggestions', array(
				'url' 			=> $this->themeUrls,
				'title'			=> $this->pageTitle,
				'metaTitle'		=> $this->metaTitle,
				'metaImage'		=> $this->metaImage,
				));
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function postSuggestions()
	{
		// get all fields submitted by form
		$input = Input::all();

		// rules required for fields to pass validation
    $rules = array(
    		'firstname'			=> 'required|min:3|max:80',
    		'lastname'			=> 'required|min:3|max:80',
      	'email'					=> 'email',
    		);

    // validate fields
  	$validator = Validator::make($input, $rules);

		// if our validation fails, do the following
		if ($validator->fails())
		{
			// get url of referring page
			$url = Request::url();

			$messages = $validator->messages();

			// redirect back to form
			return Redirect::to($url)
				->with('errorsFirstname', $this->setClass($messages, 'firstname', 'has-error'))
				->with('errorsLastname', $this->setClass($messages, 'lastname', 'has-error'))
				->with('errorsEmail', $this->setClass($messages, 'email', 'has-error'))
				->withErrors($validator);
		}
		else
		{

			// Email information
			Mail::send('emails.suggestions.suggestions', $input, function($message)
			{
				if(empty($input['email']))
				{
					$input['email'] = 'noreply@strengthandwarmth.org';
				}
			  $message->from($input['email'], 'Laravel');

			  $message->to('info@KNPcommunications.com');
			  //$message->to('jorge.calderon@colorsynthetic.com');

			  $message->subject('Name Submission from StrengthandWarmth.org');
			});
			// redirect to list page
			return Redirect::to('/')
				->with('message', 'Thank you for your suggestion.')
				->with('errorCss', 'alert-success');
		}
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	private function checkForDuplicateSubmissions($person_id)
	{
		// vote results
		$voteResults = DB::table('submissions')->select()->where('person_id', $person_id)->where('ip_address', Request::getClientIp())->first();


		if(isset($voteResults))
		{
			return false;
		}
		else
		{
			return true;
		}
	}

}
