<?php

class QuizController extends BaseController
{

	/**
	 * Setup the index page to call the quiz form
	 *
	 * @return void
	 */
	public function getIndex()
	{
		// get json output of people
		$json = Search::getJson();

		return View::make('themes.search.quiz', array(
				'url' 				=> $this->themeUrls,
				'title'				=> $this->pageTitle,
				'metaTitle'		=> $this->metaTitle,
				'metaImage'		=> $this->metaImage,
				'json'				=> $json
				));
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function postResults()
	{

		// get json output of people
		$json = Search::getJson();

		// get all input fields submitted
		$input = Input::all();

		$count = 0;
		$strength = 0;
		$warmth = 0;

		// loop through all the items and extract w and s
		foreach($input as $index => $value)
		{
			// check if value is array
			if(is_array($value))
			{
				// check the array depth
				if($this->arrayDepth($value) > 0)
				{
					// seperate values by strength or warmth
					foreach($value as $index => $v)
					{
						// check for strength values
						if($index === 's')
						{
							// add strength values
							$strength = $strength + $v;

						}
						// check for warmth values
						else if($index === 'w')
						{
							// add warmth values
							$warmth = $warmth + $v;
						}
					}
				}
			}
		}

		// calculate the average strength
		$averageStrength = number_format(((float)$strength / (count($input) - 1)), 1, '.', '');

		// calculate the average warmth
		$averageWarmth = number_format(((float)$warmth / (count($input) - 1)), 1, '.', '');

		$quote = null;

		// set up quotes
		if($averageStrength < 5.1 && $averageWarmth < 5)
		{
			$quote = array(
				'title' 	=> 'Your quiz resulted in Low Strength and Low Warmth',
				'content' => '<p>When people seem weak, we are not as concerned with what they want, because they cannot make it happen anyway. We do not pay much attention to them because they don’t command our respect. And when they seem cold, we find them unlikable. The emotions we feel when we deem someone unimportant (not strong) and unlikable (not warm) are disgust or contempt. Since we tend not to pay much attention to people who don’t matter, the public figures who wind up in this quadrant typically started somewhere else and then fell from grace, either through a scandal or a series of events that revealed something unflattering about their character.</p>
											<p>For more information about how the dynamics of strength and warmth play out in everyday life, check out <a href="http://compellingpeople.com/" target="_blank">Compelling People</a>.</p>',
				);
		}
		else if($averageStrength > 4.9 && $averageWarmth > 4.9)
		{
			$quote = array(
				'title' 	=> 'Your quiz resulted in High Strength and High Warmth',
				'content' => '<p>People who project both strength and warmth impress us as knowing what they are doing and having our best interests at heart, so we trust them and find them persuasive. They seem willing (warm) and able (strong) to look out for our interests, so we look to them for leadership and feel comfortable knowing they are in charge.</p>
											<p>For more information about how the dynamics of strength and warmth play out in everyday life, check out <a href="http://compellingpeople.com/" target="_blank">Compelling People</a>.</p>',
				);
		}
		else if($averageStrength > 4.9 && $averageWarmth < 5)
		{
			$quote = array(
				'title' 	=> 'Your quiz resulted in High Strength and Low Warmth',
				'content' => '<p>Strength can make people powerful, influential, and important; it is about getting things done. But there are things it cannot do. Strength alone can coerce, but it cannot lead. Strength for its own sake is a corrosive force. People who renounce warmth and go over to the dark side are much less likely to end up sharing their lives with people who care about them. Ironically, the stronger people are—the more easily they can bend the world to their will—the harder it is for them to be sure their friends are being friendly for the right reasons. Like spies, they trust nobody, and nobody trusts them either. At best, they inspire fear, or sometimes envy.</p>
											<p>For more information about how the dynamics of strength and warmth play out in everyday life, check out <a href="http://compellingpeople.com/" target="_blank">Compelling People</a>.</p>',
				);
		}
		else if($averageStrength < 5 && $averageWarmth > 4.9)
		{
			$quote = array(
				'title' 	=> 'Your quiz resulted in Low Strength and High Warmth',
				'content' => '<p>These folks are nice to a fault. Their judgment can be impaired by the “rose-colored glasses” syndrome, in which they are so busy seeing the best in everyone that they miss clear warning signs about people who may do them harm. Think doormat—the person in the office who continually gets passed over for promotion, the girlfriend who sticks with her jerk boyfriend even though all her friends say she should dump him, the parent who lets his kid call the shots. Old-time baseball manager Leo Durocher coined a phrase to describe what happens to them: “Nice guys finish last.” The feeling they inspire is pity.</p>
											<p>For more information about how the dynamics of strength and warmth play out in everyday life, check out <a href="http://compellingpeople.com/" target="_blank">Compelling People</a>.</p>',
				);
		}

		return View::make('themes.search.quiz-results', array(
				'url' 				=> $this->themeUrls,
				'title'				=> $this->pageTitle,
				'metaTitle'		=> $this->metaTitle,
				'metaImage'		=> $this->metaImage,
				'json'				=> $json,
				'quote'					=> $quote
				));
	}
}
