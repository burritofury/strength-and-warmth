<?php

class UserController extends BaseController
{

	/**
	 * Instantiate a new UserController instance.
	 */
	public function __construct()
	{
		// make sure user is logged in to access this controller
	  $this->beforeFilter('auth');
	}
	
	/**
	 * Setup the index page to call the login form
	 *
	 * @return void
	 */
	public function getIndex()
	{
		return View::make('hello', array('url' => $this->themeUrls));
	}
	
}