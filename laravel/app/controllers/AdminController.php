<?php

class AdminController extends BaseController
{
	public $restful = true;
	
	/**
	 * Instantiate a new UserController instance.
	 */
	public function __construct()
	{

		// make sure user is logged in to access this controller
	  $this->beforeFilter('auth');
	}

	/**
	 * Setup the index page to call the login form
	 *
	 * @return view
	 */
	public function getIndex()
	{
		$url = URL::action('AdminController@getDashboard');

		// return View
		return Redirect::to($url);
	}

	/**
	 * Setup the index page to call the login form
	 *
	 * @return view
	 */
	public function getDashboard()
	{
		// Redirect route
		return View::make('themes.search.admin.dashboard', array(
				'url' 			=> $this->themeUrls,
				'title'			=> $this->pageTitle
				));
	}
}