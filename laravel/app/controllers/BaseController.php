<?php

class BaseController extends Controller
{
	protected $themeUrls, $pageTitle, $metaTitle, $metaImage, $debug;

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		// setup data array to pass to the view
		$this->themeUrls = $this->urlPaths();
		$this->pageTitle = 'C-Score';
		$this->metaTitle = $this->pageTitle;
		$this->metaImage = 'http://compellingpeople.com/wp-content/uploads/2013/03/2013-amazon-best-books-in-business.png';

		if (!is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

	/**
	 * Setup the url paths to be used by the controller.
	 *
	 * @return array
	 */
	protected function urlPaths()
	{
		$siteVars = array(
			'base' 			=> '/',
			'url'				=> asset('/'),
			'imgUrl'		=> asset('/assets/images'),
			'cssUrl'		=> asset('/assets/css'),
			'jsUrl'			=> asset('/assets/js')
			);

		return $siteVars;
	}

	/**
	 * Setup the error classes.
	 *
	 * @return array
	 */
	protected function setClass($messages, $field, $class)
	{
		if ($messages->has($field))
		{
			return $class;
		}
	}

	/**
	 * Setup the error classes.
	 *
	 * @return array
	 */
	protected function arrayDepth(array $array) {
    $maxDepth = 1;


    foreach ($array as $value) {

      if (is_array($value)) {

        $depth = $this->arrayDepth($value) + 1;

        if ($depth > $maxDepth) {
          $maxDepth = $depth;
        }
      }
    }

    return $maxDepth;
	}
}
