<?php

class PeopleController extends BaseController {

	public 				$restful = true;

	protected			$pagination;

	/**
	 * Instantiate a new UserController instance.
	 */
	public function __construct()
	{
		// make sure user is logged in to access this controller
	  $this->beforeFilter('auth');

	  // set pagination
		$this->pagination = 5;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		// get all people in the database
		$people = DB::table('people')->select('id', 'firstname', 'lastname', 'small_path', 'created_at')->paginate($this->pagination);
		// load the view with updated data
		return View::make('themes.search.admin.people', array(
				'url' 			=> $this->themeUrls,
				'title'			=> $this->pageTitle
				))
			->with('people', $people)
			->with('paginate', $people->links());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('themes.search.admin.create', array(
				'url' 			=> $this->themeUrls,
				'title'			=> $this->pageTitle
				));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		// get all fields submitted by form
		$input = Input::all();

		// rules required for fields to pass validation
    $rules = array(
    		'firstname'			=> 'required|min:2|max:80',
    		'lastname'			=> 'required|min:2|max:80',
      	'image'					=> 'required|image|mimes:jpeg,jpg,gif,png|max:3000',
    		);

    // validate fields
  	$validator = Validator::make($input, $rules);

		// if our validation fails, do the following
		if ($validator->fails())
		{
			// get url of referring page
			$url = Request::url();

			// make sure we are sending user back to form
			if (strpos($url,'create') === false)
			{
				$url .= '/create';
			}

			$messages = $validator->messages();

			// redirect back to form
			return Redirect::to($url)
				->with('errorsFirstname', $this->setClass($messages, 'firstname', 'has-error'))
				->with('errorsLastname', $this->setClass($messages, 'lastname', 'has-error'))
				->with('errorsImage', $this->setClass($messages, 'image', 'has-error'))
				->withErrors($validator);
		}
		else
		{
			// set a new filename based on the first and last name of the person being added
			$filename = strtolower($input['firstname'] . '_' . $input['lastname']);

			// upload images and get image path
			$imagePath = Image::upload($input['image'], $filename, 'assets/images/people');

			// now let's create a small, medium, and large cropped image
			$small = Image::resizeCrop($imagePath, 'small', 50); // 50px by 50px
			$medium = Image::resizeCrop($imagePath, 'medium', 150); // 150px by 150px
			$large = Image::resizeCrop($imagePath, 'large', 300); // 300px by 300px

			if(isset($input['new_vote']) && $input['new_vote'] == 'on')
			{
				$input['new_vote'] = 1;
			}
			else
			{
				$input['new_vote'] = 0;
			}

			// insert data into the database
			$insert = DB::table('people')->insert(array(
					array('firstname' => $input['firstname'],
								'lastname' => $input['lastname'],
								'first_vote' => $input['new_vote'],
								'small_path' => $small,
								'medium_path' => $medium,
								'large_path' => $large,
								'created_at' => Carbon::now(),
								'updated_at' => Carbon::now()
								)
					));

			if(isset($input['new_vote']) && $input['new_vote'] == FALSE)
			{
				$id = DB::table('people')->where('firstname', $input['firstname'])->where('lastname', $input['lastname'])->first();

				$insertCoordinates = DB::table('coordinates')->insert(
					array(
						'person_id'			=> $id->id,
						'xAxis'					=> $input['xAxis'],
						'yAxis'					=> $input['yAxis'],
						'created_at'		=> Carbon::now(),
						'updated_at'		=> Carbon::now()
						)
				);
			}

			// delete original image
			File::delete(public_path() . $imagePath);

			// redirect to list page
			return Redirect::to('people')
				->with('message', $input['firstname'] . ' ' . $input['lastname'] . ' has just been added.')
				->with('errorCss', 'alert-success');
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		// get the person to edit
		$person = DB::table('people')->where('id', $id)->first();

		return View::make('themes.search.admin.edit', array(
		'url' 			=> $this->themeUrls,
		'title'			=> $this->pageTitle,
		'person'		=> $person
		));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		// get form field values
		$input = Input::all();

		// get data by id
		$person = People::find($id);

		// update data
		$person->firstname = $input['firstname'];
		$person->lastname = $input['lastname'];

		// set a new filename based on the first and last name of the person being added
		$filename = strtolower($input['firstname'] . '_' . $input['lastname']);

		// upload images and get image path
		$imagePath = Image::upload($input['image'], $filename, 'assets/images/people');

		// now let's create a small, medium, and large cropped image
		$person->small_path = Image::resizeCrop($imagePath, 'small', 50); // 50px by 50px
		$person->medium_path = Image::resizeCrop($imagePath, 'medium', 150); // 150px by 150px
		$person->large_path = Image::resizeCrop($imagePath, 'large', 300); // 300px by 300px

		// update table
		$person->save();

		// delete original image
		File::delete(public_path() . $imagePath);

		// redirect back to form
		return Redirect::to('/people');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		// get person information from the db
		$person = People::find($id);

		// delete images from the public directory
		File::delete(public_path() . $person->small_path);
		File::delete(public_path() . $person->medium_path);
		File::delete(public_path() . $person->large_path);

		// delete person from the database
    $person->delete();

		// redirect to list page
		return Redirect::to('people')
			->with('message', $person->firstname . ' ' . $person->lastname . ' has been permanently deleted.')
			->with('errorCss', 'alert-success');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroyImages($id)
	{
		// get person information from the db
		$person = People::find($id);

		// delete images from the public directory
		File::delete(public_path() . $person->small_path);
		File::delete(public_path() . $person->medium_path);
		File::delete(public_path() . $person->large_path);

		$person->small_path = '';
		$person->medium_path = '';
		$person->large_path = '';

		$person->save();

		// redirect to list page
		return Redirect::to('people/' . $person->id . '/edit')
			->with('message', 'Images were deleted successfully. Please add a replacement below.')
			->with('errorCss', 'alert-success');
	}

}
