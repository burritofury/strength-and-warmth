<?php

class UsersRolesTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{

    DB::table('users_roles')->delete();

    // create admin role
    UsersRoles::create(array('user_id' => 1, 'role_id' => 3));
	}
}