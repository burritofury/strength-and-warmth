<?php

class UserTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{

    DB::table('users')->delete();

    // hash test password for admin
    $password = Hash::make('12345');

    // create admin role
    User::create(array('username' => 'admin', 'email' => 'jorge.calderon@colorsynthetic.com', 'password' => $password, 'active' => 1));
	}
	
}