<?php

class RoleTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{

    DB::table('roles')->delete();

    // create admin role
    Role::create(array('role' => 'Anonymous'));
    Role::create(array('role' => 'Authenticated'));
    Role::create(array('role' => 'Administrator'));
	}
}