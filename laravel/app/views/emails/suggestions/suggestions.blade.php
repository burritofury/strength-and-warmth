<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Name Submission from StrengthandWarmth.org</h2>
			<p><strong>Suggested Person:</strong> {{ $firstname }} {{ $lastname }}</p>

			@if(!empty($email))
				<p><strong>Email address of Submitter:</strong> {{ $email }}</p>
			@endif
	</body>
</html>