@extends('themes.search.layouts.master')

@section('content')
<div class="col-md-6 col-md-offset-3">
	<h1>How We Judge Each Other</h1>
	<p>When we decide how we feel about someone, the two qualities that count are what we call strength and warmth.</p>
	<h2>Strength = the capacity to shape the world = Respect</h2>
	<p>We see strength in terms of <strong>skill</strong> and <strong>will</strong>.</p>
	<ul>
			<li>Does this person seem capable and competent?</li>
			<li>Does this person seem determined and willful?</li>
	</ul>
  <h2>Warmth = shared affinity and interests = Liking</h2>
  <p>We see warmth in terms of <strong>shared concerns or interests</strong>.</p>
  <ul>
		<li>Does this person care about people like me?</li>
		<li>Does this person share my outlook on the world?</li>
  </ul>
  <h2>Now—Judge Somebody!</h2>
  <p>Enter a (well-known) person's name in the box below to rate their strength and warmth and get their C-score: the combination that shapes how we see them.</p>
	@if(Session::has('message'))
		<div class="alert {{ Session::get('errorCss') }}">
			<a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
			{{ Session::get('message') }}
		</div>
	@endif
	{{ Form::open(array('url' => 'results', 'method' => 'get', 'id' => 'searchpeople')) }}
		<fieldset>
	  	<div class="form-group {{ Session::get('errorsLivesearch') }}">
				<!-- username field -->
				{{ Form::text('livesearch', null, array('id' => 'livesearch', 'class' => 'typeahead form-control input-lg', 'name' => 'livesearch', 'placeholder' => 'Type in a name...')) }}
			</div>
			<div class="form-group text-center">
				<!-- submit button -->
				{{ Form::button('Search', $attributes = array('class' => 'btn btn-theme', 'type' => 'submit')) }}
			</div>
		</fieldset>
	{{ Form::close() }}
<script>
	var people = {{ $json }};
</script>
@stop
