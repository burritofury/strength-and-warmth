@extends('themes.search.layouts.master')

@section('content')
<div class="col-md-6 col-md-offset-3">
	{{ Form::open(array('url' => 'results', 'method' => 'get', 'id' => 'searchpeople')) }}
		<fieldset>
	  	<div class="form-group {{ Session::get('errorsLivesearch') }}">
				<!-- username field -->
				{{ Form::text('livesearch', null, array('id' => 'livesearch', 'class' => 'typeahead form-control input-lg', 'name' => 'livesearch', 'placeholder' => 'Type in a name...')) }}
			</div>
			<div class="form-group text-center">
				<!-- submit button -->
				{{ Form::button('Search', $attributes = array('class' => 'btn btn-theme', 'type' => 'submit')) }}
			</div>
		</fieldset>
	{{ Form::close() }}

	<script>
		var people = {{ $json }};
	</script>
</div>
<div class="col-md-6 col-md-offset-3">
	<h1>{{ $quote['title'] }}</h1>
	{{ $quote['content'] }}
</div>
@stop
