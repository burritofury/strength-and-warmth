<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <!-- for Google -->
  <meta name="description" content="{{ $metaTitle }}">
  <meta name="keywords" content="{{ $metaTitle }}">
  <meta name="author" content="">
  <meta name="copyright" content="">
  <meta name="application-name" content="{{ $title }}">

  <!-- for Facebook -->
  <meta property="og:title" content="{{ $title }}">
  <meta property="og:type" content="article">
  <meta property="og:image" content="{{ $metaImage }}">
  <meta property="og:url" content="{{ Request::url() }}">
  <meta property="og:description" content="{{ $metaTitle }}">

  <!-- for Twitter -->
  <meta name="twitter:card" content="summary">
  <meta name="twitter:title" content="{{ $title }}">
  <meta name="twitter:description" content="{{ $metaTitle }}">
  <meta name="twitter:image" content="{{ $metaImage }}">

  <title>{{ $title }}</title>
  <link href="/favicon.ico" rel="icon" type="image/x-icon">

  <link rel="stylesheet" href="{{ $url['cssUrl'] }}/bootstrap.min.css">
  <link rel="stylesheet" href="{{ $url['cssUrl'] }}/style.min.css">
</head>
<body>
	<div class="top-bar">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <ul class="social-nav">
          <li><a href="https://www.facebook.com/CompellingPeople" class="facebook" title="Facebook" target="_blank"><span>Facebook</span></a></li>
          <li><a href="http://twitter.com/CompellingPeeps" class="twitter" title="Twitter" target="_blank"><span>Twitter</span></a></li>
          <li><a href="http://compellingpeople.com/contact-us/" class="feedback" title="Feedback Form" target="_blank"><span>Feedback Form</span></a></li>
          <li><a href="http://compellingpeople.com/feed/" class="rss" title="RSS Feed" target="_blank"><span>RSS Feed</span></a></li>
        </ul>
        <ul class="nav nav-pills">
            <li><a href="/">Home</a></li>
            <li><a href="http://compellingpeople.com/">Compelling People</a></li>
            @if (Auth::check())
            <li><a href="/people">People</a></li>
            @endif

        </ul>
      </div>
    </div>
	</div>
  <div class="content-container">
    <div class="row">
      @yield('content')
    </div>
  </div>

  <!-- Enable responsive features in IE8 with Respond.js (https://github.com/scottjehl/Respond) -->
  <script src="{{ $url['jsUrl'] }}/respond.min.js"></script>
  <script src="{{ $url['jsUrl'] }}/vendor/jquery-1.9.1.min.js"></script>
  <script src="{{ $url['jsUrl'] }}/vendor/jquery-ui-1.9.2.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.7.1/modernizr.min.js"></script>
  <script src="{{ $url['jsUrl'] }}/bootstrap.min.js"></script>
  <script src="{{ $url['jsUrl'] }}/typeahead/typeahead.js"></script>
  <script src="{{ $url['jsUrl'] }}/vendor/hogan.js"></script>
  <script src="{{ $url['jsUrl'] }}/vendor/highcharts.js"></script>
  <script src="{{ $url['jsUrl'] }}/highcharts.config.js"></script>
  <script src="{{ $url['jsUrl'] }}/table-fixed-header.js"></script>
  <script src="{{ $url['jsUrl'] }}/style.js"></script>

  <script>

    if($('#xAxis').length != 0) {
      $('#xAxis').slider({
        formater: function(value) {
          $('#xAxisValue').text('Current value: ' + value);
          return 'Current value: ' + value;
        }
      });
    }
    if($('#yAxis').length != 0) {
      $('#yAxis').slider({
        formater: function(value) {
          $('#yAxisValue').text('Current value: ' + value);
          return 'Current value: ' + value;
        }
      });
    }

    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-45004794-1', 'strengthandwarmth.org');
    ga('send', 'pageview');
  </script>

</body>
