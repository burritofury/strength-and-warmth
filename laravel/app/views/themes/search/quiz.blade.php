@extends('themes.search.layouts.master')

@section('content')
<div class="col-md-6 col-md-offset-3">
	{{ Form::open(array('url' => 'results', 'method' => 'get', 'id' => 'searchpeople')) }}
		<fieldset>
	  	<div class="form-group {{ Session::get('errorsLivesearch') }}">
				<!-- username field -->
				{{ Form::text('livesearch', null, array('id' => 'livesearch', 'class' => 'typeahead form-control input-lg', 'name' => 'livesearch', 'placeholder' => 'Type in a name...')) }}
			</div>
			<div class="form-group text-center">
				<!-- submit button -->
				{{ Form::button('Search', $attributes = array('class' => 'btn btn-theme', 'type' => 'submit')) }}
			</div>
		</fieldset>
	{{ Form::close() }}

	<script>
		var people = {{ $json }};
	</script>
</div>
<div class="col-md-6 col-md-offset-3">
	<h1>Take the Quiz</h1>
	<p>Please select how well each of the following words or phrases describes your personally:</p>
</div>
<div class="col-md-8 col-md-offset-2">
	{{ Form::open(array('url' => 'quiz/results', 'id' => 'quiz')) }}
	{{ Form::token() }}
	<table class="table table-striped table-hover table-responsive table-fixed-header">
		<thead class="header">
			<tr>
		    <th></th>
		    <th>Very Well</th>
		    <th>Somewhat Well</th>
		    <th>Not too Well</th>
		    <th>Not at all well</th>
		  </tr>
		</thead>
		<tbody>
		<tr>
			<th>Friendly</th>
			<td>{{ Form::radio('friendly[w]', 10)}}</td>
			<td>{{ Form::radio('friendly[w]', 8)}}</td>
			<td>{{ Form::radio('friendly[w]', 2)}}</td>
			<td>{{ Form::radio('friendly[w]', 0)}}</td>
		</tr>
		<tr>
			<th>Try to help others</th>
			<td>{{ Form::radio('help_others[w]', 10)}}</td>
			<td>{{ Form::radio('help_others[w]', 8)}}</td>
			<td>{{ Form::radio('help_others[w]', 2)}}</td>
			<td>{{ Form::radio('help_others[w]', 0)}}</td>
		</tr>
		<tr>
			<th>Likable</th>
			<td>{{ Form::radio('likable[w]', 10)}}</td>
			<td>{{ Form::radio('likable[w]', 8)}}</td>
			<td>{{ Form::radio('likable[w]', 2)}}</td>
			<td>{{ Form::radio('likable[w]', 0)}}</td>
		</tr>
		<tr>
			<th>Kind</th>
			<td>{{ Form::radio('kind[w]', 10)}}</td>
			<td>{{ Form::radio('kind[w]', 8)}}</td>
			<td>{{ Form::radio('kind[w]', 2)}}</td>
			<td>{{ Form::radio('kind[w]', 0)}}</td>
		</tr>
		<tr>
			<th>Easy to talk to</th>
			<td>{{ Form::radio('easy_talk_to[w]', 10)}}</td>
			<td>{{ Form::radio('easy_talk_to[w]', 8)}}</td>
			<td>{{ Form::radio('easy_talk_to[w]', 2)}}</td>
			<td>{{ Form::radio('easy_talk_to[w]', 0)}}</td>
		</tr>
		<tr>
			<th>Can get along with anyone</th>
			<td>{{ Form::radio('get_along_anyone[w]', 10)}}</td>
			<td>{{ Form::radio('get_along_anyone[w]', 8)}}</td>
			<td>{{ Form::radio('get_along_anyone[w]', 2)}}</td>
			<td>{{ Form::radio('get_along_anyone[w]', 0)}}</td>
		</tr>
		<tr>
			<th>A good listener</th>
			<td>{{ Form::radio('good_listener[w]', 10)}}</td>
			<td>{{ Form::radio('good_listener[w]', 8)}}</td>
			<td>{{ Form::radio('good_listener[w]', 2)}}</td>
			<td>{{ Form::radio('good_listener[w]', 0)}}</td>
		</tr>
		<tr>
			<th>Cold</th>
			<td>{{ Form::radio('cold[w]', 0)}}</td>
			<td>{{ Form::radio('cold[w]', 2)}}</td>
			<td>{{ Form::radio('cold[w]', 8)}}</td>
			<td>{{ Form::radio('cold[w]', 10)}}</td>
		</tr>
		<tr>
			<th>Get things done</th>
			<td>{{ Form::radio('get_things_done[s]', 10)}}</td>
			<td>{{ Form::radio('get_things_done[s]', 8)}}</td>
			<td>{{ Form::radio('get_things_done[s]', 2)}}</td>
			<td>{{ Form::radio('get_things_done[s]', 0)}}</td>
		</tr>
		<tr>
			<th>Assertive</th>
			<td>{{ Form::radio('assertive[s]', 10)}}</td>
			<td>{{ Form::radio('assertive[s]', 8)}}</td>
			<td>{{ Form::radio('assertive[s]', 2)}}</td>
			<td>{{ Form::radio('assertive[s]', 0)}}</td>
		</tr>
		<tr>
			<th>Well respected</th>
			<td>{{ Form::radio('well_respected[s]', 10)}}</td>
			<td>{{ Form::radio('well_respected[s]', 8)}}</td>
			<td>{{ Form::radio('well_respected[s]', 2)}}</td>
			<td>{{ Form::radio('well_respected[s]', 0)}}</td>
		</tr>
		<tr>
			<th>Confident</th>
			<td>{{ Form::radio('confident[s]', 10)}}</td>
			<td>{{ Form::radio('confident[s]', 8)}}</td>
			<td>{{ Form::radio('confident[s]', 2)}}</td>
			<td>{{ Form::radio('confident[s]', 0)}}</td>
		</tr>
		<tr>
			<th>Decisive</th>
			<td>{{ Form::radio('decisive[s]', 10)}}</td>
			<td>{{ Form::radio('decisive[s]', 8)}}</td>
			<td>{{ Form::radio('decisive[s]', 2)}}</td>
			<td>{{ Form::radio('decisive[s]', 0)}}</td>
		</tr>
		<tr>
			<th>Easily persuaded by others</th>
			<td>{{ Form::radio('easily_persuaded[s]', 0)}}</td>
			<td>{{ Form::radio('easily_persuaded[s]', 2)}}</td>
			<td>{{ Form::radio('easily_persuaded[s]', 8)}}</td>
			<td>{{ Form::radio('easily_persuaded[s]', 10)}}</td>
		</tr>
		<tr>
			<th>Weak</th>
			<td>{{ Form::radio('weak[s]', 10)}}</td>
			<td>{{ Form::radio('weak[s]', 8)}}</td>
			<td>{{ Form::radio('weak[s]', 2)}}</td>
			<td>{{ Form::radio('weak[s]', 0)}}</td>
		</tr>
		<tr>
			<th>A leader</th>
			<td>{{ Form::radio('leader[s]', 10)}}</td>
			<td>{{ Form::radio('leader[s]', 8)}}</td>
			<td>{{ Form::radio('leader[s]', 2)}}</td>
			<td>{{ Form::radio('leader[s]', 0)}}</td>
		</tr>
		<tr>
			<th>Attract the attention of other people</th>
			<td>{{ Form::radio('attract_attention_others[s]', 10)}}</td>
			<td>{{ Form::radio('attract_attention_others[s]', 8)}}</td>
			<td>{{ Form::radio('attract_attention_others[s]', 2)}}</td>
			<td>{{ Form::radio('attract_attention_others[s]', 0)}}</td>
		</tr>
		<tr>
			<th>Worth listening to</th>
			<td>{{ Form::radio('worth_listening_to[s]', 10)}}</td>
			<td>{{ Form::radio('worth_listening_to[s]', 8)}}</td>
			<td>{{ Form::radio('worth_listening_to[s]', 2)}}</td>
			<td>{{ Form::radio('worth_listening_to[s]', 0)}}</td>
		</tr>
		<tr>
			<th>Admired</th>
			<td>{{ Form::radio('admired[s]', 10)}}</td>
			<td>{{ Form::radio('admired[s]', 8)}}</td>
			<td>{{ Form::radio('admired[s]', 2)}}</td>
			<td>{{ Form::radio('admired[s]', 0)}}</td>
		</tr>
		</tbody>
	</table>
	<div class="form-group text-center">
		<!-- submit button -->
		<button class="btn btn-theme" type="submit">Submit Answers</button>
	</div>
	{{ Form::close() }}
</div>
@stop
