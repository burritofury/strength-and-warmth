@extends('themes.search.layouts.master')

@section('content')
<div class="col-md-6 col-md-offset-3">

	{{ Form::open(array('url' => 'suggestions')) }}
	{{ Form::token() }}
		<fieldset>
    	<legend>Suggest a Person</legend>

	  	<div class="form-group {{ Session::get('errorsFirstname') }}">
				{{ Form::label('firstname', 'First Name', array('class' => 'control-label')) }}
				{{ Form::text('firstname', null, array('placeholder' => 'Enter the first name of the person you are suggesting.', 'class' => 'form-control')) }}
				@if ($errors->has('firstname'))
				  <p class="help-block">{{ $errors->first('firstname'); }}</p>
				@endif
			</div>

	  	<div class="form-group {{ Session::get('errorsLastname') }}">
				{{ Form::label('lastname', 'Last Name', array('class' => 'control-label')) }}
				{{ Form::text('lastname', null, array('placeholder' => 'Enter the last name of the person you are suggesting.', 'class' => 'form-control')) }}
				@if ($errors->has('lastname'))
				  <p class="help-block">{{ $errors->first('lastname'); }}</p>
				@endif
			</div>


	  	<div class="form-group {{ Session::get('errorsEmail') }}">
				{{ Form::label('email', 'E-Mail', array('class' => 'control-label')) }}
				{{ Form::text('email', null, array('placeholder' => 'Optional: Enter your email address', 'class' => 'form-control')) }}
				@if ($errors->has('lastname'))
				  <p class="help-block">{{ $errors->first('email'); }}</p>
				@endif
			</div>

			<div class="form-group">
				<!-- submit button -->
				{{ Form::button('Send us your suggestion', $attributes = array('class' => 'btn btn-theme', 'type' => 'submit')) }}
			</div>
		</fieldset>
	{{ Form::close() }}
</div>
@stop