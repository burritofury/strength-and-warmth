@extends('themes.search.layouts.master')

@section('content')
<div class="col-md-offset-4 col-md-4">
	{{ Form::open(array('url' => 'login', 'role' => 'form', 'class' => 'form-horizontal')) }}
		<fieldset>
			<legend>Login</legend>
			@if (Session::has('flashError'))
        <div class="alert alert-danger" role="alert">{{ Session::get('flashError') }}</div>
    	@endif
	  	<div class="form-group {{ Session::get('errorsUsername') }}">
				<!-- username field -->
				{{ Form::label('username', 'Username', array('class' => 'control-label')) }}
				{{ Form::text('username', null, array('placeholder' => 'Enter your username', 'class' => 'form-control')) }}
				@if ($errors->has('username'))
					<p class="help-block">{{ $errors->first('username') }}</p>
				@endif
			</div>

	  	<div class="form-group {{ Session::get('errorsPassword') }}">
				<!-- password field -->
				{{ Form::label('password', 'Password', array('class' => 'control-label')) }}
				{{ Form::password('password', array('class' => 'form-control')) }}
				@if ($errors->has('password'))
					<p class="help-block">{{ $errors->first('password') }}</p>
				@endif
			</div>

	  	<div class="form-group">
				{{ Form::checkbox('loggedin', 0) }}
				{{ Form::label('loggedin', 'Remember me')}}
			</div>

			<div class="form-group">
				<!-- submit button -->
				{{ Form::button('Login', $attributes = array('class' => 'btn btn-theme btn-block', 'type' => 'submit')) }}
			</div>
		</fieldset>
	{{ Form::close() }}

</div>
@stop
