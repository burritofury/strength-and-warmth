@extends('themes.search.layouts.master')

@section('content')
<div class="col-md-6 col-md-offset-3">
	{{ Form::open(array('url' => 'results', 'method' => 'get', 'id' => 'searchpeople')) }}
		<fieldset>
	  	<div class="form-group {{ Session::get('errorsLivesearch') }}">
				<!-- username field -->
				{{ Form::text('livesearch', null, array('id' => 'livesearch', 'class' => 'typeahead form-control input-lg', 'name' => 'livesearch', 'placeholder' => 'Type in a name...')) }}
			</div>
			<div class="form-group text-center">
				<!-- submit button -->
				{{ Form::button('Search', $attributes = array('class' => 'btn btn-theme', 'type' => 'submit')) }}
			</div>
		</fieldset>
	{{ Form::close() }}

	<script>
		var people = {{ $json }};
	</script>
</div>
<div class="col-md-6 col-md-offset-3 clearfix">
	<img src="{{ $person->medium_path }}" class="img-gutter-right img-thumbnail pull-left">
	<h1>{{ $person->firstname . ' ' . $person->lastname }}</h1>
	<blockquote>
		<p class="blue instructions">How do you feel about this person? Use the sliders to vote below.</p>
	</blockquote>
</div>

<div class="col-md-6 col-md-offset-3">
	@if ($errors->has('xAxis'))
		<div class="alert alert-danger">
			<p>{{ $errors->first('xAxis') }}</p>
		</div>
	@endif
</div>

	{{ Form::open(array('url' => 'results/vote')) }}
	{{ Form::hidden('id', $person->id) }}
	<!-- {{ Form::hidden('xAxis', null, array('id' => 'xAxis')) }} -->
	<!-- {{ Form::hidden('yAxis', null, array('id' => 'yAxis')) }} -->
	{{ Form::token() }}

	<div class="col-md-6 col-md-offset-3">
		<br>
		<div class="form-group sliders">
			<div id="yAxisValue" class="values"></div>
			{{ Form::label('yAxis', 'Weak', array('class' => 'control-label')) }}
			{{ Form::label('yAxis', 'Strong', array('style' => 'float:right', 'class' => 'control-label')) }}
			{{ Form::text('yAxis', null, array('data-slider-min' => '0', 'data-slider-max' => '10', 'data-slider-value' => '-10', 'data-slider-step' => '.5', 'id' => 'yAxis')) }}
		</div>
		<div class="form-group sliders">
			<div id="xAxisValue" class="values"></div>
			{{ Form::label('xAxis', 'Cold', array('class' => 'control-label')) }}
			{{ Form::label('xAxis', 'Warm', array('style' => 'float:right', 'class' => 'control-label')) }}
			{{ Form::text('xAxis', null, array('data-slider-min' => '0', 'data-slider-max' => '10', 'data-slider-value' => '-10', 'data-slider-step' => '.5', 'id' => 'xAxis')) }}
		</div>
		<!-- <div id="chart"></div> -->
	</div>
	<div class="col-md-6 col-md-offset-3">
		<div class="form-group">
			{{ Form::label('comment', 'Comment', array('class' => 'control-label')) }}
			{{ Form::text('comment', null, array('placeholder' => 'Leave a comment', 'class' => 'comment form-control')) }}
			<div  id="counter"></div>
		</div>
		<div class="form-group">
			<!-- submit button -->
			{{ Form::button('Vote', $attributes = array('class' => 'btn btn-theme btn-block', 'type' => 'submit')) }}
			<p class="text-center view-results"><a href="{{ URL::to('results/average/' . $person->id) }}">See results</a></p>
		</div>
	{{ Form::close() }}
</div>
@stop