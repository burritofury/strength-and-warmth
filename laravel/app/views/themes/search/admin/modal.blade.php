<div class="modal" id="personIdNum{{ $person->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">You are about to delete {{ $person->firstname . ' ' . $person->lastname }}</h4>
      </div>
      <div class="modal-body">
        <p>Are you sure that you wish to delete {{ $person->firstname . ' ' . $person->lastname }}? <strong>This action cannot be undone.</strong></p>
      </div>
      <div class="modal-footer">
        {{ Form::open(array('route' => array('people.destroy', $person->id), 'method' => 'delete')) }}
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-danger">Delete {{ $person->firstname . ' ' . $person->lastname }}</button>
        {{ Form::close() }}
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->