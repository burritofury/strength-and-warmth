@extends('themes.search.layouts.master')

@section('content')
<div class="col-md-8 col-md-offset-2">
	<h1>People</h1>

	{{ link_to('people/create', 'Add People', $attributes = array('class' => 'btn btn-success pull-right'), $secure = null) }}
	@if(Session::has('message'))
		<div class="alert {{ Session::get('errorCss') }} clearfix">
			<a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
			{{ Session::get('message') }}
		</div>
	@endif

	{{-- Form::open(array('url' => 'people')) --}}
	@if (! $people->isEmpty())
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th colspan="2"></th>
					<th>Name</th>
					<th>Date Added</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
			@foreach($people as $person)
				<tr>
					<td>{{-- $person['order'] --}}</td>
					<td><img class="img-thumbnail" src="{{ $person->small_path }}"></td>
					<td>{{ $person->firstname . ' ' . $person->lastname}}</td>
					<td>{{ Carbon::createFromFormat('Y-m-d H:i:s', $person->created_at)->toFormattedDateString() }}</td>
					<td class="actions">
					  <a type="button" class="btn btn-info btn-xs" href="{{ URL::to('people', array($person->id, 'edit')) }}">Edit</a>
					  <button type="button" class="btn btn-danger btn-xs" data-toggle="modal" href="#personIdNum{{ $person->id }}" >Delete</button>

					  @include('themes.search.admin.modal', array('people' => $person))
					</td>
				</tr>
			@endforeach
			</tbody>
			<tfoot>
				<tr>
					<td colspan="5">{{ $paginate }}</td>
				</tr>
			</tfoot>
		</table>
	@else
		<p>You have not added any people. Add your first one <a href="{{ URL::to('people', array('create')) }}">here</a>.</p>
	@endif
	{{-- Form::close() --}}
</div>
@stop
