@extends('themes.search.layouts.master')

@section('content')
<div class="col-md-6 col-md-offset-3">

	{{ Form::open(array('url' => 'people', 'files' => true)) }}

		<fieldset>
    	<legend>People</legend>

	  	<div class="form-group {{ Session::get('errorsFirstname') }}">
				<!-- username field -->
				{{ Form::label('firstname', 'First Name', array('class' => 'control-label')) }}
				{{ Form::text('firstname', null, array('placeholder' => 'Enter the first name of the person.', 'class' => 'form-control')) }}
				@if ($errors->has('firstname'))
				  <p class="help-block">{{ $errors->first('firstname'); }}</p>
				@endif
			</div>

	  	<div class="form-group {{ Session::get('errorsLastname') }}">
				<!-- username field -->
				{{ Form::label('lastname', 'Last Name', array('class' => 'control-label')) }}
				{{ Form::text('lastname', null, array('placeholder' => 'Enter the last name of the person.', 'class' => 'form-control')) }}
				@if ($errors->has('lastname'))
				  <p class="help-block">{{ $errors->first('lastname'); }}</p>
				@endif
			</div>

	  	<div class="form-group">
				<!-- username field -->
				{{ Form::label('xAxis', 'X Axis', array('class' => 'control-label')) }}
				{{ Form::text('xAxis', null, array('data-slider-min' => '0', 'data-slider-max' => '10', 'data-slider-value' => '-10', 'data-slider-step' => '.5', 'id' => 'xAxis')) }}
			</div>
	  	<div class="form-group">
				{{ Form::label('yAxis', 'Y Axis', array('class' => 'control-label')) }}
				{{ Form::text('yAxis', null, array('data-slider-min' => '0', 'data-slider-max' => '10', 'data-slider-value' => '-10', 'data-slider-step' => '.5', 'id' => 'yAxis')) }}
			</div>

	  	<div class="form-group {{ Session::get('errorsImage') }}">
				{{ Form::label('image', 'Image', array('class' => 'control-label')) }}
				{{ Form::file('image'); }}
				@if ($errors->has('image'))
				  <p class="help-block">{{ $errors->first('image'); }}</p>
				@endif
			</div>

	  	<div class="form-group {{ Session::get('errorsVote') }}">
				{{ Form::label('new_vote', 'Do Not Register First Vote', array('class' => 'control-label')) }}
				{{ Form::checkbox('new_vote', NULL); }}
			</div>
			<div class="form-group">
				<!-- submit button -->
				{{ Form::button('Add', $attributes = array('class' => 'btn btn-theme', 'type' => 'submit')) }}
			</div>
		</fieldset>
	{{ Form::close() }}
</div>
@stop
