@extends('themes.search.layouts.master')

@section('content')
<div class="col-md-8 col-md-offset-2">

	{{ Form::open(array('route' => array('people.update', $person->id), 'files' => true)) }}

		<fieldset>
    	<legend>Person</legend>

	  	<div class="form-group {{ Session::get('errorsFirstname') }}">
				<!-- username field -->
				{{ Form::label('firstname', 'First Name', array('class' => 'control-label')) }}
				{{ Form::text('firstname', $person->firstname, array('placeholder' => 'Enter the first name of the person.', 'class' => 'form-control')) }}
				@if ($errors->has('firstname'))
				  <p class="help-block">{{ $errors->first('firstname'); }}</p>
				@endif
			</div>

	  	<div class="form-group {{ Session::get('errorsLastname') }}">
				<!-- username field -->
				{{ Form::label('lastname', 'Last Name', array('class' => 'control-label')) }}
				{{ Form::text('lastname', $person->lastname, array('placeholder' => 'Enter the last name of the person.', 'class' => 'form-control')) }}
				@if ($errors->has('lastname'))
				  <p class="help-block">{{ $errors->first('lastname'); }}</p>
				@endif
			</div>

	  	<div class="form-group {{ Session::get('errorsImage') }}">
				{{ Form::label('image', 'Image', array('class' => 'control-label')) }}
				<div class="images">
					@if (! empty($person->large_path))
						{{ HTML::image($person->large_path, $person->firstname . ' ' . $person->lastname, array('class' => 'img-thumbnail')) }}
						{{ HTML::image($person->medium_path, $person->firstname . ' ' . $person->lastname, array('class' => 'img-thumbnail')) }}
						{{ HTML::image($person->small_path, $person->firstname . ' ' . $person->lastname, array('class' => 'img-thumbnail')) }}

						<button type="button" class="btn btn-danger btn-xs" data-toggle="modal" href="#personIdNum{{ $person->id }}" >Delete Images</button>
						@include('themes.search.admin.modal-image', array('people' => array('id' => $person->id, 'name' => $person->firstname . ' ' . $person->lastname)))
					@endif
				</div>
				{{ Form::file('image'); }}
				@if ($errors->has('image'))
				  <p class="help-block">{{ $errors->first('image'); }}</p>
				@endif
			</div>

			<div class="form-group">
				<!-- submit button -->
				{{ Form::button('Edit', $attributes = array('class' => 'btn btn-theme', 'type' => 'submit')) }}
			</div>
		</fieldset>
	{{ Form::close() }}
</div>
@stop
