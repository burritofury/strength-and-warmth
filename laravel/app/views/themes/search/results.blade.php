@extends('themes.search.layouts.master')

@section('content')
<div class="col-md-6 col-md-offset-3">
	{{ Form::open(array('url' => 'results', 'method' => 'get', 'id' => 'searchpeople')) }}
		<fieldset>
	  	<div class="form-group {{ Session::get('errorsLivesearch') }}">
				<!-- username field -->
				{{ Form::text('livesearch', null, array('id' => 'livesearch', 'class' => 'typeahead form-control input-lg', 'name' => 'livesearch', 'placeholder' => 'Type in a name...')) }}
			</div>
			<div class="form-group text-center">
				<!-- submit button -->
				{{ Form::button('Search', $attributes = array('class' => 'btn btn-theme', 'type' => 'submit')) }}
			</div>
		</fieldset>
	{{ Form::close() }}

	<script>
		var people = {{ $json }};
		var xAxis = {{ $averageX }};
		var yAxis = {{ $averageY }};
		var allVotes = {{ $coordinates }};
	</script>

@if(Session::has('message'))
	<div class="alert {{ Session::get('errorCss') }}">
		<a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
		{{ Session::get('message') }}
	</div>
@endif
</div>

<div class="col-md-6 col-md-offset-3">
	<img src="{{ $person->medium_path }}" class="img-gutter-right img-thumbnail pull-left">
	<h1>{{ $person->firstname . ' ' . $person->lastname }}</h1>
	<blockquote>
		<p>
		@if(!is_null($quote['url']))
			<a href="{{ $quote['url'] }}" target="_blank">
		@endif
				{{ $quote['title'] }}
				<p><a href="/results/{{ $person->id }}/{{ strtolower($person->firstname . ' ' . $person->lastname) }}">Add your vote!</a></p>
		@if(!is_null($quote['url']))
			</a>
		@endif
		</p>
		@if(!is_null($quote['url']))
		<p class="blue instructions">How do we respond emotionally to strength and warmth <span class="glyphicon glyphicon-question-sign"></span></p>
		@endif
	</blockquote>
	<p>
</div>
<div class="col-md-6 col-md-offset-3">
</div>
	<div class="col-md-6 col-md-offset-3">
		<div id="chartAverage"></div>
	</div>
</div>
@stop
