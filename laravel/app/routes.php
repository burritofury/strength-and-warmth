<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(array(
	'domain' => 'www.strengthandwarmth.org'
	),
	function()
	{
		Route::get('json/people.json', 'SearchController@getPeopleJson');
		Route::get('results/average/{id}', 'SearchController@getAverageVotes');

		Route::get('results/{id}/{name?}', 'SearchController@getResults');
		Route::post('results/vote', 'SearchController@postResults');


		Route::post('quiz/results', 'QuizController@postResults');

		Route::controller('quiz', 'QuizController');

		Route::controller('/', 'SearchController');

		Route::filter('csrf', function()
		{
		  if (Request::forged()) return Response::error('500');
		});
	}
);

Route::group(array(
	'domain' => 'strengthandwarmth.org'
	),
	function()
	{
		Route::get('json/people.json', 'SearchController@getPeopleJson');
		Route::get('results/average/{id}', 'SearchController@getAverageVotes');

		Route::get('results/{id?}/{name?}', 'SearchController@getResults');
		Route::post('results/vote', 'SearchController@postResults');


		Route::controller('/', 'SearchController');

		// Route::controller('quiz', 'QuizController');

		Route::filter('csrf', function()
		{
		    if (Request::forged()) return Response::error('500');
		});
	}
);

// Login required routes
Route::group(array(
	'domain' 	=> 'admin.strengthandwarmth.dev'
	),
	function()
	{
		// Resource routes
		Route::get('people/{id}/destroy/images', 'PeopleController@destroyImages');

		Route::post('people/{id}', 'PeopleController@update');
		Route::resource('people', 'PeopleController');

		// Login and logout routes
		Route::get('login', array('uses' => 'AuthController@getLogin'));
		Route::post('login', array('uses' => 'AuthController@postLogin'));
		Route::get('logout', array('uses' => 'AuthController@getLogout'));

		Route::controller('users', 'UserController');
		Route::controller('/', 'AdminController');
	}
);

Route::group(array(
	'domain' => 'www.strengthandwarmth.dev'
	),
	function()
	{
		Route::get('json/people.json', 'SearchController@getPeopleJson');
		Route::get('results/average/{id}', 'SearchController@getAverageVotes');

		Route::get('results/{id?}/{name?}', 'SearchController@getResults');
		Route::post('results/vote', 'SearchController@postResults');

		Route::post('quiz/results', 'QuizController@postResults');

		Route::controller('quiz', 'QuizController');

		Route::controller('/', 'SearchController');

		Route::filter('csrf', function()
		{
		    if (Request::forged()) return Response::error('500');
		});
	}
);

// Route::group(array(
// 	'domain' => 'strengthandwarmth.dev'
// 	),
// 	function()
// 	{
// 		Route::get('json/people.json', 'SearchController@getPeopleJson');
// 		Route::get('results/average/{id}', 'SearchController@getAverageVotes');

// 		Route::get('results/{id?}/{name?}', 'SearchController@getResults');
// 		Route::post('results/vote', 'SearchController@postResults');

// 		Route::controller('/', 'SearchController');

// 		// Route::controller('quiz', 'QuizController');

// 		Route::filter('csrf', function()
// 		{
// 		    if (Request::forged()) return Response::error('500');
// 		});
// 	}
// );

// // Login required routes
// Route::group(array(
// 	'domain' 	=> 'admin.strengthandwarmth.dev'
// 	),
// 	function()
// 	{
// 		// Resource routes
// 		Route::get('people/{id}/destroy/images', 'PeopleController@destroyImages');

// 		Route::post('people/{id}', 'PeopleController@update');
// 		Route::resource('people', 'PeopleController');

// 		// Login and logout routes
// 		Route::get('login', array('uses' => 'AuthController@getLogin'));
// 		Route::post('login', array('uses' => 'AuthController@postLogin'));
// 		Route::get('logout', array('uses' => 'AuthController@getLogout'));

// 		Route::controller('users', 'UserController');
// 		Route::controller('/', 'AdminController');
// 	}
// );
