<?php

class UsersRoles extends Eloquent
{

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	public function users()
	{
		return $this->belongsTo('User');
	}
}