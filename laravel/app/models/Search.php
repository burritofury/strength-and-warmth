<?php

class Search extends Eloquent
{
	protected $table = 'search';

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public static function getJson()
	{
		// get data
		$people = DB::table('people')
			->leftJoin('coordinates', 'people.id', '=', 'coordinates.person_id')
			->select('people.id', 'people.firstname', 'people.lastname', 'people.small_path', 'coordinates.xAxis')
			->groupBy('coordinates.person_id')
			->get();

		foreach($people as $id => $person)
		{
			$p[$id]['id'] = $person->id;
			$p[$id]['name'] = $person->firstname . ' ' . $person->lastname;
			$p[$id]['small_path'] =  $person->small_path;
			$p[$id]['xAxis'] =  $person->xAxis;
		}

		$people = $p;

		// return data encoded with json
		return json_encode($people);
	}

}
